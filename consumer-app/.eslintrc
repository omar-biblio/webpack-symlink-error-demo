{
  "extends": "eslint-config-airbnb",
  "rules": {
    // This is broken with the current version not sure where the problem is either in babel-eslint or newer eslint itself
    "comma-dangle": [2, 'never'],
    "key-spacing": 0,
    "no-duplicate-case": 0,
    "quotes": [
      2, "single", "avoid-escape"
    ],
    "new-cap": [2, {
      "capIsNewExceptions": [
        // ImmutableJS Exceptions
        "Map",
        "List",
        "OrderedMap",
        "Set",
        "OrderedSet",
        "Range",
        "Repeat",
        "Record",
        "Seq",
        "Iterable",
        "Collection"
      ]
    }],
    "space-before-function-paren": 0,
    "import/no-named-as-default": "off",
    "import/no-extraneous-dependencies": "off",
    "import/first": "off",
    "max-len": ["error", 200, 2, {
      "ignoreUrls": true,
      "ignoreComments": false,
      "ignoreRegExpLiterals": true,
      "ignoreStrings": true,
      "ignoreTemplateLiterals": true,
    }],
    "no-console": "error",

    // New Rules as warnings
    "jsx-a11y/href-no-hash": ["error", ["a"]],

    "jsx-quotes": [2, "prefer-double"],
    "multi-spaces": 0,
    "arrow-parens": [2, "as-needed"],
    "no-underscore-dangle": 0,
    "no-confusing-arrow": 0,
    "mocha/no-exclusive-tests": "error",
    "react/jsx-closing-bracket-location": [2, "after-props"],
    "react/sort-comp": [2, {
      order: [
        'static-methods',
        'lifecycle',
        'everything-else',
        'rendering',
      ],
      groups: {
        rendering: [
          '/^render.+$/',
          'render'
        ],
        static-methods: [
          'propTypes',
          'fetchInitialData'
        ]
      }
    }]
  },
  "globals": {
    "__DEVEVELOPMENT__": true,
    "__CLIENT__": true,
    "__SERVER__": true,
    "__DISABLE_SSR__": true,
    "webpackIsomorphicTools": true
  },
  "env": {
      "es6": true,
      "node": true,
      "browser": true,
      "mocha": true
  },
  "plugins": [
      "react",
      "jsx-a11y",
      "mocha"
  ]
}
