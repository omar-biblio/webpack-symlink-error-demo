#!/usr/bin/env node
var _ = require('lodash'),
    fs = require('fs');

var statsFiles = ['widget-stats.json', 'app-stats.json'];
var outputFileName = 'webpack-stats.json';

var files = _.map(statsFiles, function (fileName) {
  return JSON.parse(fs.readFileSync(fileName, 'utf-8'));
});

fs.writeFile(outputFileName, JSON.stringify(_.merge.apply(null, files)), function(err) {
  if (err) {
    throw('Error merging webpack-stats');
  }
  console.log('Merged webpack-stats.json');
});
