module.exports = {
  plugins: {
    autoprefixer: {
      browsers: ['last 2 version', 'ie >= 9', 'iOS >= 8']
    }
  }
};
