var path = require('path');
var webpack = require('webpack');
var HappyPack = require('happypack');
var fs = require('fs');
var loaders = require('./shared/loaders');
var resolve = require('./shared/resolve');
var production = process.env.NODE_ENV === 'production';
//var nodeExternals =  require('webpack-node-externals');
var nodeModules = {
  'react-dom/server': 'commonjs react-dom/server'
};

process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';

fs.readdirSync('./node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });


const serverLoaders = [
  //loaders.json,
  loaders.images,
  loaders.exposeJQuery,
  loaders.exposeHandlebars
];

const serverPlugins = [
  new webpack.NormalModuleReplacementPlugin(/\.s?css$/, 'node-noop'),
  new webpack.IgnorePlugin(/(\.map$)|vertx|farmhash/),
  new webpack.BannerPlugin({
    banner: 'require("source-map-support").install();',
    raw: true,
    entryOnly: false }),
  new webpack.DefinePlugin({
    __CLIENT__: false,
    __SERVER__: true,
    __DEVELOPMENT__: !production,
    __DISABLE_SSR__: false
  })
];

if (production) {
  serverLoaders.unshift(
    loaders.jsx_prod
  );
} else {
  serverLoaders.unshift(
    loaders.happypack_jsx
  );

  serverPlugins.unshift(
    new HappyPack({
      id: 'babel',
      threads: 4,
      loaders: [loaders.jsx_dev]
    })
  );
}

module.exports = {
  devtool: '#source-map', // The most accurate source map. Alternatives: https://webpack.github.io/docs/configuration.html#devtool
  context: path.resolve(__dirname, '..'),
  target: 'node',
  externals: nodeModules,
  entry: ['babel-polyfill', './server/index.js'],
  output: {
    path: path.join(__dirname, '../build'),
    filename: 'server.js'
  },
  module: {
    rules: serverLoaders
  },
  node: {
    __filename: true,
    __dirname: true
  },
  // progress: true,
  resolve:  resolve,
  plugins: serverPlugins

};
