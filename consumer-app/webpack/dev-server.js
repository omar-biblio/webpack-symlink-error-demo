var webpack          = require('webpack'),
    WebpackDevServer = require('webpack-dev-server'),
    webpackConfig    = require('./client-build/dev');

var compiler   = webpack(webpackConfig),
    host       = process.env.HOST || 'localhost',
    port       = parseInt(process.env.PORT) + 1 || 3002,
    serverOpts = {
      contentBase: 'http://' + host + ':' + port,
      quiet:       false,
      //colors:      true,
      noInfo:      true,
      //hot:         true,
      inline:      true,
      lazy:        false,
      publicPath:  webpackConfig.output.publicPath,
      headers:     { 'Access-Control-Allow-Origin': '*' },
      stats:       { colors: true }
    };

new WebpackDevServer(compiler, serverOpts)
.listen(port, function onAppListening(err) {
  if (err) {
    console.error(err);
  } else {
    console.info('==> 🚧  Webpack development server listening on %s:%s', host, port);
  }
});
