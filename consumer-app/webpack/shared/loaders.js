var strip             = require('strip-loader');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
var _                 = require('lodash');


// Assets (images, fonts, etc) smaller than this size (in bytes)
// will be inlined (data URI) rather than linked to.
var inlinedAssetSizeLimit = '10240';

// Supported browsers for autoprefixer to use
const postcssConfig = path.resolve(process.cwd(), 'postcss.config.js');

module.exports = {
  happypack_jsx: {
    use: ['react-hot-loader/webpack', 'happypack/loader?id=babel'], // 'react-hot-loader/webpack',
    test:    /\.jsx?$/,
    exclude: /node_modules/,
    include: /app|server/
  },

  jsx_dev: {
    loader:  'babel-loader',
    query: {
      cacheDirectory: true
    }
  },

  jsx_prod: {
    test:    /\.jsx?$/,
    exclude: /node_modules/,
    include: /app|server/,
    loader: 'babel-loader'
  },

  eslint: {
    test: /\.jsx?$/,
    loader: 'eslint-loader',
    exclude: /node_modules/,
    include: /app|server/
  },

  scss_dev: {
    test:   /\.s?css$/,
    use: [
      'style-loader',
      {
        loader:'css-loader',
        options: {
          importLoaders: 2,
          sourceMap: true
        }
      },
      { 
        loader: 'postcss-loader',
        options: { sourceMap: true, config: { path: postcssConfig } }
      },
      {
        loader: 'sass-loader',
        options: {
          outputStyle: 'expanded',
          sourceMap: true
        }
      }]
  },

  scss_prod: {
    test:   /\.s?css$/,
    // The "-autoprefixer" option for css is required to work around a bug with Webpack/UglifyJS:
    // See https://github.com/postcss/autoprefixer/issues/660#issuecomment-220808316
    //loader: ExtractTextPlugin.extract('style', 'css?-autoprefixer&importLoaders=2&sourceMap!autoprefixer?' + supportedBrowsers + '!sass?outputStyle=expanded')
    use: ExtractTextPlugin.extract(
      {
        fallback: 'style-loader',
        use: [
          {
            loader:'css-loader',
            options: {
              importLoaders: 2,
              sourceMap: true
            }
          },
          // 'css-loader?-autoprefixer&importLoaders=2&sourceMap', // ?-autoprefixer&importLoaders=2&sourceMap
          //'postcss-loader', // ?' + supportedBrowsers,
          { loader: 'postcss-loader', options: { sourceMap: true, config: { path: postcssConfig } } },
          // 'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'expanded',
              sourceMap: true
            }
          }
        ] // ?outputStyle=expanded
        // use: 'css-loader?-autoprefixer&importLoaders=2&sourceMap!postcss-loader?' + supportedBrowsers + '!sass-loader?outputStyle=expanded'
      })
  },

  images: {
    test:   /\.(png|jpg|gif|ico|woff|woff2|eot|ttf|svg)$/,
    loader: 'url-loader?limit=' + inlinedAssetSizeLimit
  },

  exposeJQuery: {
    test: require.resolve('jquery'),
    loader: 'expose-loader?$!expose-loader?jQuery'
  },

  exposeHandlebars: {
    test: /handlebars\.js/,
    loader: 'expose-loader?Handlebars'
  }
};
