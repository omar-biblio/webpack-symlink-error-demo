module.exports = [
  'raf/polyfill',
  'babel-polyfill',
  'immutable',
  'iso',
  'jquery',
  'lodash',
  'moment',
  'react',
  'react-router',
  'redux',
  'react-redux',
  'superagent',
  'when',
  'rxjs',

  // Dependencies of Core External Header
  'handlebars'
];
