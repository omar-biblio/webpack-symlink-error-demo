var path = require('path');

module.exports = {
  // root: [
  //   path.resolve('./app'),
  //   path.resolve('./node_modules'),
  //   path.resolve('./vendor')
  // ],
  // fallback: path.resolve(__dirname, '../../node_modules'),
  // alias: {
  //   // Use the web build of handlebars instead of the node build
  //   handlebars: 'handlebars/dist/handlebars.js',
  //   containers: path.resolve(__dirname, '../../app/containers'),
  //   constants: path.resolve(__dirname, '../../app/constants'),
  //   actions: path.resolve(__dirname, '../../app/actions')
  // },
  // extensions: ['', '.json', '.js', '.jsx']

  symlinks: true,
  modules: [
    path.resolve('./app'),
    path.resolve('./node_modules'),
    path.resolve('./vendor'),
    // path.resolve(__dirname, '../../node_modules'),
  ],
  alias: {
    // Use the web build of handlebars instead of the node build
    // 'webpack-hot-client/client': path.resolve(...),
    handlebars: 'handlebars/dist/handlebars.js',
    containers: path.resolve(__dirname, '../../app/containers'),
    constants: path.resolve(__dirname, '../../app/constants'),
    actions: path.resolve(__dirname, '../../app/actions')
  },
  // extensions: ['.js', '.jsx', '.json']
  extensions: ['.js', '.jsx']
};
