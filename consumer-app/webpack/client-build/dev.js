var _ = require('lodash');
var path    = require('path');
var webpack = require('webpack');

var WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
var webpackIsomorphicToolsConfig = require('../shared/webpack-isomorphic-tools');

var loaders = require('../shared/loaders');
var resolve = require('../shared/resolve');
var vendors = require('../shared/vendors');

var assetsPath = path.resolve(__dirname, '../..', 'public/');
var host       = process.env.HOST || 'localhost';
var port       = parseInt(process.env.PORT, 10) + 1 || 3002;
var HappyPack         = require('happypack');
var nodeExternals =  require('webpack-node-externals');


const widgetBuild = process.env.WIDGET_BUILD === 'true';
const appEntries = [
  'webpack-dev-server/client?http://' + host + ':' + port,
  'webpack/hot/only-dev-server',
  './app/bootstrap.js'
];

module.exports = {
  devtool: 'eval', // The fastest source map. Alternatives: https://webpack.github.io/docs/configuration.html#devtool
  context: path.resolve(__dirname, '../..'),
  mode: 'development',
  // devServer: {
  //   headers: {
  //     'Access-Control-Allow-Origin': '*'
  //   },
  //   // disableHostCheck: true,
  //   hot: true,
  //   watchOptions: {
  //     poll: true
  //   }
  // },
  entry: _.extend(widgetBuild ? { widget: appEntries, 'widget-vendors': vendors } : { app: appEntries, vendors: vendors }),
  externals: [
    'farmhash',
    // nodeExternals({
    //   // modulesFromFile: true,
    //   whitelist: [/\.(?!(?:jsx?|json)$).{1,5}$/i]
    // })
  ],
  output: {
    path:          assetsPath,
    filename:      '[name].js',
    chunkFilename: '[name].js',
    publicPath:    'http://' + host + ':' + port + '/'
  },
  module: {
    rules: [
      loaders.happypack_jsx,
      loaders.eslint,
      loaders.scss_dev,
      //loaders.json,
      loaders.images,
      loaders.exposeJQuery,
      loaders.exposeHandlebars
    ]
  },
  // sassLoader: {
  //   data: `$is-widget: ${widgetBuild ? 'true' : 'false'};`
  // },
  // eslint: {
  //   emitWarning: true
  // },
  // progress: true,
  resolve:  resolve,
  // resolveLoader: {
  //   moduleExtensions: ["-loader"]
  // },
  // resolveLoader: {
  //   fallback: path.resolve(__dirname, '../../node_modules')
  // },
  plugins: [
    // hot reload
    new HappyPack({
      id: 'babel',
      threads: 4,
      loaders: [loaders.jsx_dev]
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: true,
      __DISABLE_SSR__: false
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        sassLoader: {
          data: `$is-widget: ${widgetBuild ? 'true' : 'false'};`
        },
        eslint: {
          emitWarning: true
        },
        progress: true
      }
    }),

    // https://github.com/halt-hammerzeit/webpack-isomorphic-tools
    new WebpackIsomorphicToolsPlugin(webpackIsomorphicToolsConfig).development()
  ]
};
