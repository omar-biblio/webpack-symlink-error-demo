module.exports = [
  require('./client-build/prod')(false),
  require('./client-build/prod')(true),
  require('./rails-modules/prod')(false),
  require('./server-config')
];
