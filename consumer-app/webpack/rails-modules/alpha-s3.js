var S3Plugin   = require('webpack-s3-plugin');

var output     = require('./output-alpha');
var prodConfig = require('./alpha')(true);

var s3Credentials = require('../shared/s3-credentials');

var s3 = new S3Plugin({
  // Exclude uploading of html
  exclude: /.*\.html$/,
  // s3Options are required
  s3Options: s3Credentials,
  s3UploadOptions: {
    Bucket: 'big-assets/react-shared-components'
  },
  noCdnizer: true
});

// Same as the prod build for the rails modules, except the modules are named
// with hash fingerprints and uploaded to S3.
module.exports = Object.assign({}, prodConfig, {
  output:  output.hash,
  plugins: [].concat(s3, prodConfig.plugins)
});
