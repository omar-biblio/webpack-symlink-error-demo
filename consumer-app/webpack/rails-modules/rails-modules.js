var path    = require('path');

var entry   = require('./entry');
var loaders = require('../shared/loaders');
var resolve = require('../shared/resolve');

var nodeExternals =  require('webpack-node-externals');

module.exports = {
  devtool: 'source-map',
  context: path.resolve(__dirname, '../..'),
  module:  {
    rules: [
      loaders.jsx_prod,
      //loaders.json,
      loaders.scss_prod,
      loaders.images
    ]
  },
  // progress: true,
  resolve,
  entry,
  externals: [{
    // Use external version of React
    // 'react': 'React',
    // 'react-addons-css-transition-group': 'React.addons.CSSTransitionGroup',
    // 'react-addons-transition-group': 'React.addons.TransitionGroup',
    // 'lodash': '_',
    // 'jquery': 'jQuery',
  },
  nodeExternals({
    whitelist: [/\.(?!(?:jsx?|json)$).{1,5}$/i]
  })
]
};
