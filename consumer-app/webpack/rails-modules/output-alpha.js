var path = require('path');
var args = require('yargs').argv;

// asset path
var relativeAssetsPath = '../../public/';
var assetsPath         = path.join(__dirname, relativeAssetsPath);

module.exports = {
  hash: {
    path:          assetsPath,
    filename:      '[name]-' + args.buildversion + '.' + args.buildnumber + '.js',
    chunkFilename: '[name]-' + args.buildversion + '.' + args.buildnumber + '.js',
    libraryTarget: 'var',
    library:       '[name]',
    publicPath:    '/v2/'
  },

  nohash: {
    path:          assetsPath,
    filename:      '[name].js',
    chunkFilename: '[name].js',
    libraryTarget: 'var',
    library:       '[name]',
    publicPath:    '/v2/'
  }
};
