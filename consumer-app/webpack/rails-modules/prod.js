var webpack = require('webpack');

// Plugins
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanPlugin       = require('clean-webpack-plugin');

var output             = require('./output');
var railsModulesConfig = require('./rails-modules');

// asset path
var relativeAssetsPath = '../../public/';

module.exports = function(hash) {
  return Object.assign({}, railsModulesConfig, {
    output:  output.nohash,
    plugins: [
      new CleanPlugin([relativeAssetsPath]),

      // css files from the extract-text-plugin loader
      new ExtractTextPlugin(hash ? '[name]-[chunkhash].css' : '[name].css', {allChunks: true}),
      new webpack.DefinePlugin({
        // Only CLIENT is true because the compiled code is sent to the browser and run on the client.
        __CLIENT__:      true,
        __SERVER__:      false,
        __DEVELOPMENT__: false,
        __DISABLE_SSR__: false
      }),

      // ignore dev config
      new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),

      // set global vars
      new webpack.DefinePlugin({
        'process.env': {
          // Useful to reduce the size of client-side libraries, e.g. react
          NODE_ENV: JSON.stringify('production')
        }
      }),

      // optimizations
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  });
};

