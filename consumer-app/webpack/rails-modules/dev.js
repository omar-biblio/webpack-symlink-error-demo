var webpack            = require('webpack');
var ExtractTextPlugin  = require('extract-text-webpack-plugin');

var output             = require('./output');
var railsModulesConfig = require('./rails-modules');

module.exports = Object.assign({}, railsModulesConfig, {
  mode: 'development',
  output: output.nohash,
  // devServer: {
  //   headers: {
  //     'Access-Control-Allow-Origin': '*'
  //   },
  //   // disableHostCheck: true,
  //   hot: true,
  //   watchOptions: {
  //     poll: true
  //   }
  // },
  plugins: [
    new ExtractTextPlugin({filename: '[name].css', allChunks: true }),
    new webpack.DefinePlugin({
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: true,
      __DISABLE_SSR__: false
    })
  ]
});
