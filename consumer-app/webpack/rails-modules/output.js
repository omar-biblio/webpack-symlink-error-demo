var path    = require('path');

// asset path
var relativeAssetsPath = '../../public/';
var assetsPath         = path.join(__dirname, relativeAssetsPath);


module.exports = {
  hash: {
    path:          assetsPath,
    filename:      '[name]-[chunkhash].js',
    chunkFilename: '[name]-[chunkhash].js',
    libraryTarget: 'var',
    library:       '[name]',
    publicPath:    '/v2/'
  },

  nohash: {
    path:          assetsPath,
    filename:      '[name].js',
    chunkFilename: '[name].js',
    libraryTarget: 'var',
    library:       '[name]',
    publicPath:    '/v2/'
  }
};
