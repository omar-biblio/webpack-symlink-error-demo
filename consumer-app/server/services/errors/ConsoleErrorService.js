import debugFrom from 'debug';
const debug = debugFrom('ui-core:server:services:errors:ConsoleErrorService');

const reject = () => {};
const resolve = () => {};

/**
 * Implements an ErrorService that reports to the console. Ideal as a local-dev
 * stub for the usual airbrake reporter.
 */
class ConsoleErrorService {

  /**
   * Chirps on the console.
   *
   * @param  {Error|any} err - the Error to log.
   * @param  {object} req    - the incoming express request
   * @return {Promise}
   */
  notify(err, req) { // eslint-disable-line class-methods-use-this
    debug('notify()');

    if (!err) return reject(new Error('Undefined err'));
    // Not used, but assert the interface
    if (!req) return reject(new Error('Undefined req'));

    console.error(err); // eslint-disable-line no-console
    // Log the stack trace to process.stderr -- console.error doesn't print \n properly
    process.stderr.write(`\n${err.stack || '[no stack trace]'}\n`);

    return resolve('[logged to console]');
  }
}

export default ConsoleErrorService;
