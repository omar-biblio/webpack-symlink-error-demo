import config from 'config';

import AirbrakeErrorService from './AirbrakeErrorService';
import ConsoleErrorService from './ConsoleErrorService';

const airbrake = config.get('airbrake') || {};
const airbrakeConfig = { ...airbrake.clientSettings, ...airbrake.server };

const errorService = airbrakeConfig.enabled ?
        new AirbrakeErrorService(airbrakeConfig) :
        new ConsoleErrorService();

export default errorService;
