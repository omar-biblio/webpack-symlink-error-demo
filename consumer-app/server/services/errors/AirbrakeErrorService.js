import airbrake from 'airbrake';
import debugFrom from 'debug';
import _ from 'lodash';

const debug = debugFrom('ui-core:server:services:errors:AirbrakeErrorService');

const EXCLUDED_PARAMS = ['username', 'password'];
const EXCLUDED_COOKIES = ['remember_me'];

const EXCLUDED_ROUTES = ['/v2/healthcheck'];

/**
 * Implements an ErrorService that reports to airbrake.io.
 */
class AirbrakeErrorService {
  constructor({ projectId, projectKey, env, appVersion }) {
    if (projectKey) {
      this.client = airbrake.createClient(projectId, projectKey);
      this.client.appVersion = appVersion;
      this.client.env = env;
      this.client.timeout = 3000;

      this.addParamFilter();
      this.addRouteFilter();
    } else {
      debug('cannot initialize airbrake client: undefined projectKey');
    }
  }

  /**
   * Extends `err` with diagnostic info for airbrake to log. Mutates `err`.
   * @param  {Error|object} err
   * @param  {object} req
   * @return {Error|object}
   */
  static decorateError(err, req) {
    if (!err) return new Error('Undefined err');
    if (!req) return new Error('Undefined req');

    const decoratedError = err;
    const cookies = _.omitBy(req.cookies, (value, key) => _.includes(EXCLUDED_COOKIES, key));
    decoratedError.url = err.url || `${req.protocol}://${req.get('host')}${req.originalUrl}`;
    decoratedError.component = err.component || _.get(req, 'route.path') || req.get('host');
    decoratedError.action = err.action || req.method;
    decoratedError.params = err.params || req.body;
    decoratedError.session = err.session || req.session;
    decoratedError.session = { ...(decoratedError.session || {}), cookies };
    return err;
  }

  addParamFilter() {
    this.client.addFilter(notice => {
      /* eslint-disable no-param-reassign */
      notice.params = _.transform(notice.params, (result, value, key) => {
        result[key] = _.includes(EXCLUDED_PARAMS, key) ? '[FILTERED]' : value;
      }, {});

      return notice;
    });
  }

  addRouteFilter() {
    this.client.addFilter(notice => _.includes(EXCLUDED_ROUTES, notice.context.component) ? null : notice);
  }

  /**
   * Logs `err` to airbrake.io.
   *
   * @param  {Error|any} err - the Error to log
   * @param  {object} req    - the incoming express request
   * @return {Promise}       - resolves with the airbrake error ID
   */
  notify(err, req) {
    debug('notify()');
    return new Promise((resolve, reject) => {
      if (!err) return reject(new Error('Undefined err'));
      if (!req) return reject(new Error('Undefined req'));
      if (!this.client) return reject(new Error('Undefined airbrake client'));

      return this.client.notify(AirbrakeErrorService.decorateError(err, req), (notifyErr, url) => {
        if (notifyErr || !url) return reject(notifyErr); // Rejects on filtered errors

        // Record the issue ID in the server logs, to support searching for it
        console.error(`Error reported to Airbrake: ${url}`);  // eslint-disable-line no-console
        debug(url);

        // Get the error id from notice url
        const segments = url ? url.split('/') : [];
        const errorId = segments[segments.length - 1];

        return resolve(errorId);
      });
    })
    .catch(error => {
      // Failed to alert airbrake; make noise on other available channels before
      // handing back the rejection.
      debug(error);
      console.error(error); // eslint-disable-line no-console
      throw error;
    });
  }
}

export default AirbrakeErrorService;
