/* eslint-disable */
/**
 * Module dependencies.
 */


// use strict to allow use of const on server.
"use strict";

//require('newrelic');

// Node.js has the Intl APIs built-in, but only includes the English locale data by default.
// Use the Intl.js polyfill, which contains all the locales that we need.
global.Intl = require('intl');

const fs = require('fs');
const path = require('path');
const rootDir = path.resolve(__dirname, '..');
// const config = require('config');

const WebpackIsomorphicTools = require('webpack-isomorphic-tools');
global.webpackIsomorphicTools = new WebpackIsomorphicTools(require('../webpack/shared/webpack-isomorphic-tools'))
  .server(rootDir, () => {
    const app = require('./main').default;
    const debug = require('debug')('ui-core:server');
    const http = require('http');

    /**
    * Normalize a port into a number, string, or false.
    */

    function normalizePort(val) {
      const portNumber = parseInt(val, 10);

      if (isNaN(portNumber)) {
        // named pipe
        return val;
      }

      if (portNumber >= 0) {
        // portNumber number
        return portNumber;
      }

      return false;
    }


    /**
    * Get port from environment and store in Express.
    */

    const port = normalizePort(process.env.PORT || '3001');
    app.set('port', port);

    /**
    * Create HTTP server.
    */
    const server = http.createServer(app);

    /**
    * Event listener for HTTP server "error" event.
    */

    function onError(error) {
      if (error.syscall !== 'listen') {
        throw error;
      }

      const bind = typeof port === 'string'
        ? `Pipe ${port}`
        : `Port ${port}`;

      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          console.error(`${bind} requires elevated privileges`); // eslint-disable-line no-console
          break;
        case 'EADDRINUSE':
          console.error(`${bind} is already in use`); // eslint-disable-line no-console
          break;
        default:
          break;
      }
      throw error;
    }

    /**
    * Event listener for HTTP server "listening" event.
    */

    function onListening() {
      const addr = server.address();
      const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
      debug(`Listening on ${bind}`);
    }

    /**
    * Listen on provided port, on all network interfaces.
    */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
  });
