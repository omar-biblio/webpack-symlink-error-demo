import bodyParser from 'body-parser';
// import config from 'config';
import cookieParser from 'cookie-parser';
import debugFrom from 'debug';
import express from 'express';
import exphbs from 'express-handlebars';
import morgan from 'morgan';
import noCache from 'nocache';
import path from 'path';
import errorHandler from 'errorhandler';

import logErrors from './middlewares/logErrors';


const debug = debugFrom('ui-core:server:main');

debug('initializing');
const server = express();

// **** View Engine Setup ****
const hbs = exphbs.create({
  defaultLayout: 'app',
  layoutsDir: path.join(__dirname, 'views/layouts')
});

server.set('views', path.join(__dirname, 'views'));
server.engine('handlebars', hbs.engine);
server.set('view engine', 'handlebars');

// **** Middleware Setup ****
server.use(cookieParser());
// server.use(contextInitializer);
// server.use(morgan('dev', { stream: logger.stream }));
// server.use(detectDomain);
// server.use(protocol);
// server.use(hsts);
// server.use(version);
// server.use(ieCompat);
server.use(noCache());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

server.enable('trust proxy');

server.use('/v2',
  express.static(path.join(__dirname, '../public')),
  express.static(path.join(__dirname, 'assets'))
);

// **** Routes ****
debug('defining routes');
// server.get(`${'/v2'}/gc`, collectGarbage);
// server.get(`${'/v2'}/healthcheck`, healthcheck);
// server.get(`${'/v2'}/info/version`, getVersion);
// server.get(`${'/v2'}/500`, testError);

// React App
server.get(
  `/v2/*`,
  // coreStyles,
  // detectLocale,
  // detectLocalBranch,
  // detectMobileApp,
  // detectWidgetApp,
  // setupApiClient,
  // detectLocalBranchByIp,
  // getRenderProps,
  // setupReduxStore,
  // setTransactionName,
  // setupBrowserMonitoring,
  // renderLibraryError,
  // renderReact
);

// **** Error Middlewares ****
server.use(
  '/v2',
  logErrors,
  errorHandler()
  // config.get('discloseErrors') ? errorHandler() : internalErrors
);

export default server;
