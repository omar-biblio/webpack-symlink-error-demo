/* eslint-disable global-require */
/* eslint-disable react/jsx-filename-extension */
import config from 'config';
import debugFrom from 'debug';
import Iso from 'iso';
import mapValues from 'lodash/mapValues';
import { Provider } from 'react-redux';
import { RouterContext } from 'react-router';

import { getDataLayer } from '../../app/selectors/AnalyticsSelector';
import { currentLibrarySelector } from '../../app/selectors/LibrarySelector';
import { currentAccountSelector, currentUserSelector } from '../../app/selectors/AuthSelector';

const React = require('react');
const ReactDOMServer = require('react-dom/server');
const Helmet = require('react-helmet');

const debug = debugFrom('ui-core:server:middlewares:renderReact');

const assetsRootURL = config.assetsRootURL || '';
const assetsUseCORS = !!config.assetsUseCORS;

/**
 * Renders the React client on the server (aka server-side rendering)
 * NOTE: This middleware is the terminator of all app requests, hence, does not call `next()`.
 *
 * @implements {ExpressMiddleware}
 */
export default function renderReact(req, res) {
  debug('renderReact()');

  // Do not cache webpack stats: the script file would change since
  // hot module replacement is enabled in the development env
  if (config.get('refreshAssetsOnRequest')) webpackIsomorphicTools.refresh();

  const {
    reduxStore,
    renderProps,
    coreCssFingerprint,
    browserMonitoringScript,
    widgetApp
  } = req.appRequestContext;

  const reduxState = reduxStore.getState();
  const currentUser = currentUserSelector(reduxState);
  const currentAccount = currentAccountSelector(reduxState);

  const anyRouteRequiresAuthentication = renderProps.components.some(component => component.requiresAuthentication);
  const anyRouteRequiresAccount = renderProps.components.some(component => component.requiresAccount);

  if ((anyRouteRequiresAuthentication && !currentUser) || (anyRouteRequiresAccount && !currentAccount)) {
    const location = renderProps.location;
    const redirectUrl = currentUser ? '/user_dashboard' : `/user/login?destination=${encodeURIComponent(`${location.pathname}${location.search}`)}`;
    return res.redirect(302, redirectUrl);
  }

  const notFoundRoute = renderProps.routes.find(route => route.path === '*');
  if (notFoundRoute) {
    res.status(404);
  }

  debug('rendering react app');

  const rootComponentHTML = __DISABLE_SSR__ ?
                            '<div></div>' :
                            ReactDOMServer.renderToString(
                              <Provider store={reduxStore}>
                                <RouterContext {...renderProps} />
                              </Provider>
                            );

  const { cdnHost, cssPath, defaultStylesheet } = config.coreAssets;
  const currentLibrary = currentLibrarySelector(reduxState);
  const dataLayer = getDataLayer(reduxState);
  const siteId = currentLibrary.get('siteId');
  const head = mapValues(Helmet.rewind(), tag => tag.toString());

  return res.render('app/index', {
    rootComponentHTML: (new Iso()).add(rootComponentHTML, reduxState.toJS()).render(),
    coreDefaultCSS: `${cdnHost}${cssPath}/${siteId}/${defaultStylesheet}?${coreCssFingerprint}`,
    currentLocale: reduxState.getIn(['localization', 'currentLanguage'], config.defaultLocale),
    coreJavascripts: reduxState.getIn(['templates', 'js']),
    coreCSS: reduxState.getIn(['templates', 'css']),
    assets: global.webpackIsomorphicTools.assets(),
    gtmContainerId: config.gtmContainerId,
    gtmContainerAuth: config.gtmContainerAuth,
    gtmContainerPreview: config.gtmContainerPreview,
    dataLayer: JSON.stringify(dataLayer.toJS()),
    gaTrackingId: config.gaTrackingId,
    faviconUrl: currentLibrary.get('faviconUrl'),
    useWidgetAssets: !!widgetApp,
    browserMonitoringScript,
    assetsRootURL,
    assetsUseCORS,
    siteId,
    head
  });
}
