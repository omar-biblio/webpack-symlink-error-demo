import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:detectLocalBranchByIp');

const COOKIE_MAX_AGE = 15 * 60 * 1000; // 15mins (in milliseconds)

export default function detectLocalBranchByIp(req, res, next) {
  debug('detectLocalBranchByIp()');

  const { appRequestContext, ip } = req;

  if (!appRequestContext) {
    return next(new Error('undefined: req.appRequestContext must be defined'));
  }

  const { apiClient, libraryDomain, sharedCookieDomain, localBranch } = appRequestContext;
  if (localBranch) {
    return next();
  }

  return apiClient.get(`/libraries/${libraryDomain}/branches`, { query: { ip } })
  .toPromise()
  .then(response => {
    const branches = response.body.branches || {};
    const branchCode = branches.items ? branches.items[0] : null;
    if (branchCode) {
      appRequestContext.localBranch = branchCode;
      debug(`localBranch: ${appRequestContext.localBranch}`);

      // Cache the detected localBranch in a cookie, to speed up subsequent lookups.
      // See the `detectLocalBranch` middleware.
      res.cookie(
        'branch',
        JSON.stringify({ ip: `${req.ip}`, [libraryDomain]: appRequestContext.localBranch }),
        { domain: sharedCookieDomain, maxAge: COOKIE_MAX_AGE }
      );
    }

    next();
  })
  .catch(next);
}
