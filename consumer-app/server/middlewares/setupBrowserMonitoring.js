import debugFrom from 'debug';
import newrelic from 'newrelic';

const debug = debugFrom('ui-core:server:middlewares:setupBrowserMonitoring');

export default function setupBrowserMonitoring(req, res, next) {
  debug('setupBrowserMonitoring()');

  const { appRequestContext, cookies, sharedCookieDomain } = req;

  let useNRBrowserMonitoring = cookies.use_new_relic === 'true';

  if (typeof cookies.use_new_relic === 'undefined') {
    useNRBrowserMonitoring = Math.random() >= 0.75;
    res.cookie('use_new_relic', useNRBrowserMonitoring.toString(), { domain: sharedCookieDomain });
  }

  Object.assign(appRequestContext, {
    browserMonitoringScript: useNRBrowserMonitoring ? newrelic.getBrowserTimingHeader() : ''
  });

  next();
}
