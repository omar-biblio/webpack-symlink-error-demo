import config from 'config';
import debugFrom from 'debug';
import httpClient from 'superagent';
import ApiClient from '../../app/api/ApiClient';

const debug = debugFrom('ui-core:server:middlewares:setupApiClient');

export default function setupApiClient(req, res, next) {
  debug('setupApiClient()');

  const { currentLocale, libraryDomain, protocol } = req.appRequestContext;
  const apiGatewayURLForServer = `${config.apiGatewayHostServer}${config.basePath}`;

  const apiRequestHeaders = {
    cookie: req.headers.cookie,
    'x-forwarded-for': req.ip,
    'x-requested-with': req.headers['x-requested-with'],
    'user-agent': req.headers['user-agent'],
    referer: `${protocol}://${req.headers.host}${req.originalUrl}`
  };

  const apiClient = new ApiClient(httpClient, {
    subdomain: libraryDomain,
    locale: currentLocale,
    apiGatewayURL: apiGatewayURLForServer,
    headers: apiRequestHeaders
  });

  Object.assign(req.appRequestContext, { apiClient });
  next();
}
