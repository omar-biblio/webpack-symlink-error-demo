import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:testError');

/**
 * Testing tool. Puts an error on the middleware chain to test how the stack handles it.
 *
 * @implements {ExpressMiddleware}
 */
export default function testError(req, res, next) {
  debug('throwing');
  next(new Error('Test Error'));
}
