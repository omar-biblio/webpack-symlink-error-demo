import debugFrom from 'debug';
import { getLibraryError } from '../../app/selectors/ErrorsSelector';

const debug = debugFrom('ui-core:server:middlewares:renderLibraryError');

/**
 * Renders various templates for when a library error occurs.
 */
export default function renderLibraryError(req, res, next) {
  debug('checking for library errors');

  const { reduxStore } = req.appRequestContext;
  const libraryError = getLibraryError(reduxStore.getState());

  if (!libraryError) {
    return next();
  }

  const { status, error } = libraryError.toJS();
  switch (status) {
    // Unknown subdomain
    case 404: {
      return res
        .status(404)
        .render('errors/404', {
          layout: 'error',
          title: 'Page not found'
        });
    }

    // Site disabled
    case 410: {
      return res
        .status(410)
        .render('errors/410', {
          layout: 'error',
          title: 'Site Disabled',
          error
        });
    }

    default:
      return next(error);
  }
}
