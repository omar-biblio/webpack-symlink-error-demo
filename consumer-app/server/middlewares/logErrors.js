import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:logErrors');

export default function logErrors(err, req, res, next) {
  debug('logErrors()');

  // logger.error(err);
  next(err);
}
