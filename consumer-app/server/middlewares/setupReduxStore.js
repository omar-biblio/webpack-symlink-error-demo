import config from 'config';
import debugFrom from 'debug';
import cookie from 'cookie';
import Immutable from 'immutable';

// import createReduxStore from '../../app/helpers/redux/createReduxStore';

const debug = debugFrom('ui-core:server:middlewares:setupReduxStore');
const appVersion = 1;

export default function setupReduxStore(req, res, next) {
  debug('setupReduxStore()');

  const {
    apiClient,
    libraryDomain,
    localBranch,
    renderProps,
    mobileApp,
    featureFlags
  } = req.appRequestContext;

  const airbrake = config.get('airbrake') || {};
  const enableWebpackHMR = config.get('enableWebpackHMR');
  const parsedCookies = cookie.parse(req.headers.cookie || '');

  // NOTE: This should only contain browser-safe configs, thus no sensitive information!
  const appConfig = {
    airbrakeConfig: { ...airbrake.clientSettings, ...airbrake.browser },
    apiGatewayURL: `${config.apiGatewayHost}${config.basePath}`,
    basePath: config.basePath,
    coreAssets: config.get('coreAssets'),
    gaTrackingId: config.gaTrackingId,
    allowFeatureFlagEditing: !!config.get('allowFeatureFlagEditing'),
    enableReduxDevTools: !!config.get('enableReduxDevTools'),
    enableWebpackHMR,
    libraryDomain,
    localBranch,
    mobileApp,
    featureFlags,
    appVersion
  };

  // Create the store and dispatch an inital set of config properties to it
  // const reduxStore = createReduxStore(Immutable.Map(), apiClient, enableWebpackHMR);
  // reduxStore.dispatch(AppActions.loadAppConfig(appConfig));
  // reduxStore.dispatch(AppActions.loadCookies(parsedCookies));

  // RoutingUtils.fetchDataForCurrentRoute(renderProps, reduxStore)
  // .then(() => {
  //   Object.assign(req.appRequestContext, { reduxStore });
  //   next();
  // })
  // .catch(next);
}
