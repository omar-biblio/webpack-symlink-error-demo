import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:ieCompat');

export default function ieCompat(req, res, next) {
  debug('ieCompat()');
  res.set('X-UA-Compatible', 'IE=Edge,chrome=1');
  next();
}
