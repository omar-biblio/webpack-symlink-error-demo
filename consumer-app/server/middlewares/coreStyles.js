const coreCssFingerprint = `${Date.now()}`;

/**
 * Defines stuff to support the import of Core styles.
 *
 * @param {object} req.appRequestContext - Must be defined by upstream middleware.
 */
export default function coreStyles(req, res, next) {
  if (!req.appRequestContext) return next(new Error('undefined req.appRequestContext'));

  Object.assign(req.appRequestContext, { coreCssFingerprint });
  return next();
}
