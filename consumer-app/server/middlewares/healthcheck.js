import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:healthcheck');

export default function healthcheck(req, res) {
  debug('healthcheck()');

  return res.status(200).send('OK');
}
