import moment from 'moment';
import errorService from '../services/errors';

/**
 * Renders a generic 500 message and logs `err` to the reporting service.
 * Errors with non-500 HTTP status should have been handled before this point.
 *
 * This middleware is the terminator for all errors in the request chain. It does
 * not call next().
 *
 * @implements {ExpressErrorMiddleware}
 */
export default function internalErrors(err, req, res, next) { // eslint-disable-line no-unused-vars
  const timestamp = moment().format('YYYY/MM/DD HH:mm');
  const title = 'The page is temporarily unavailable';

  errorService
    .notify(err, req)
    .then(id =>
      res
        .status(500)
        .render('errors/500', { error: { id }, layout: 'error', title, timestamp })
    )
    .catch(() => {
      // The error service failed also. It's not your day. Log and go flip a desk.
      console.error(err); // eslint-disable-line no-console
      res
        .status(500)
        .render('errors/500', { error: { id: null }, layout: 'error', title, timestamp });
    });
}
