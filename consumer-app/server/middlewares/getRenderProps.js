import config from 'config';
import debugFrom from 'debug';
import * as reactRouter from 'react-router';
import routes from '../../app/routes/Routes';

const debug = debugFrom('ui-core:server:middlewares:getRenderProps');

/**
 * Determines the `renderProps` needed for server-side rendering.
 * `renderProps` contain the matched components, params, location, etc.
 *
 * @implements {ExpressMiddleware}
 */
export default function getRenderProps(req, res, next) {
  debug('getRenderProps()');

  reactRouter.match({ routes: routes(config.basePath), location: req.originalUrl }, (err, redirectLocation, renderProps) => {
    if (err) {
      return next(err);
    }

    if (redirectLocation) {
      return res.redirect(302, `${redirectLocation.pathname}${redirectLocation.search}`);
    }

    Object.assign(req.appRequestContext, { renderProps });
    return next();
  });
}
