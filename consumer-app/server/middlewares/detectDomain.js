import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:detectDomain');

/**
 * Defines `host` and `libraryDomain` on the current appRequestContext.
 *
 * @param {object} req.appRequestContext - Must be defined by upstream middleware.
 */
export default function detectDomain(req, res, next) {
  debug('detecting domain');
  const { appRequestContext, query } = req;
  const { host } = req.headers;

  if (!appRequestContext) return next(new Error('undefined req.appRequestContext'));

  Object.assign(appRequestContext, {
    host,
    libraryDomain: query.domain || (host && host.split('.')[0]),
    sharedCookieDomain: host ? `.${host.split('.').slice(1).join('.')}` : ''
  });

  return next();
}
