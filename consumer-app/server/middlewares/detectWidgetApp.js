import config from 'config';
import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:detectWidgetApp');

export default function detectWidgetApp(req, res, next) {
  debug('detectWidgetApp()');

  const { appRequestContext } = req;

  Object.assign(appRequestContext, {
    widgetApp: req.originalUrl.includes(`${config.basePath}/widgets`)
  });

  next();
}
