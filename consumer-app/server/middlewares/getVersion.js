import debugFrom from 'debug';

const appVersion = 1;

const debug = debugFrom('ui-core:server:middlewares:getVersion');

export default function getVersion(req, res) {
  debug('getVersion()');

  res.status(200);
  return res.json({ version: appVersion });
}
