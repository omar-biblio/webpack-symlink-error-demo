import debugFrom from 'debug';
import compact from 'lodash/compact';
import trimStart from 'lodash/trimStart';
import newrelic from 'newrelic';
import path from 'path';

const debug = debugFrom('ui-core:server:middlewares:setTransactionName');

export default function setTransactionName(req, res, next) {
  debug('setTransactionName()');

  const { appRequestContext } = req;

  const routePaths = compact(appRequestContext.renderProps.routes.map(route => route.path));
  const currentRoutePath = trimStart(path.join(...routePaths), '/');
  newrelic.setTransactionName(currentRoutePath);

  next();
}
