import debugFrom from 'debug';

const debug = debugFrom('ui-core:server:middlewares:detectLocale');

/**
 * Middleware for determining the request locale from query or cookies.
 * Note that this would not be a validated language code and may not be supported by the current library.
 */
export default function detectLocale(req, res, next) {
  debug('detectLocale()');
  const { appRequestContext, query, cookies } = req;

  if (!appRequestContext) {
    return next(new Error('undefined req.appRequestContext'));
  }

  appRequestContext.currentLocale = query.locale || cookies.language;
  if (appRequestContext.currentLocale && appRequestContext.currentLocale !== cookies.language) {
    res.cookie('language', appRequestContext.currentLocale);
  }

  return next();
}
