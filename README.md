
# Webpack 4 Symlink Dependancy Error Demo
This repo simulates a resolve error that I've been getting with my application since I've upgraded from Webpack 1 to Webpack 3/4.

To get around this error, I could install the missing dependancy in the consuming application but I'm looking for a more scalable solution; ideally, Webpack should be able to resolve dependancies from symlinked modules.

**Note** This demo is only meant to showcase the build error outputted from Webpack to the nodejs process and does not run correctly in the browser. The goal of this demo is to resolve the build error.

## Steps to reproduce

 Mac:
````
$> git clone https://omar-biblio@bitbucket.org/omar-biblio/webpack-symlink-error-demo.git
$> cd webpack-symlink-error-demo/lerna-repo
$> npm install
$> lerna bootstrap --hoist
$> cd packages/package-foo
$> npm link
$> cd ../../../consumer-app
$> npm install
$> npm link @omar-webpack-test/package-foo
$> npm start
````

After Webpack server has been started, react-dates (which is a dependancy of the symlinked @omar-webpack-test/package-foo which is imported in app/bootstrap.js) cannot be resolved which throws a build error from node:

```
../lerna-repo/packages/package-foo/lib/index.js
Module not found: Error: Can't resolve 'react-dates/initialize' in '/Users/omarplummer/Repos/webpack-symlink-error-demo/lerna-repo/packages/package-foo/lib'
resolve 'react-dates/initialize' in '/Users/omarplummer/Repos/webpack-symlink-error-demo/lerna-repo/packages/package-foo/lib'
  Parsed request is a module
  using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/lerna-repo/packages/package-foo/package.json (relative path: ./lib)
    Field 'browser' doesn't contain a valid alias configuration
    resolve as module
      looking for modules in /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app
        using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./app)
          Field 'browser' doesn't contain a valid alias configuration
          using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./app/react-dates/initialize)
            no extension
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize doesn't exist
            .js
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize.js doesn't exist
            .jsx
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize.jsx doesn't exist
            as directory
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize doesn't exist
      looking for modules in /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules
        using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./node_modules)
          Field 'browser' doesn't contain a valid alias configuration
          using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./node_modules/react-dates/initialize)
            no extension
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize doesn't exist
            .js
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize.js doesn't exist
            .jsx
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize.jsx doesn't exist
            as directory
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize doesn't exist
      looking for modules in /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor
        using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./vendor)
          Field 'browser' doesn't contain a valid alias configuration
          using description file: /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/package.json (relative path: ./vendor/react-dates/initialize)
            no extension
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize doesn't exist
            .js
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize.js doesn't exist
            .jsx
              Field 'browser' doesn't contain a valid alias configuration
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize.jsx doesn't exist
            as directory
              /Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize doesn't exist
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize.js]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/app/react-dates/initialize.jsx]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize.js]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/node_modules/react-dates/initialize.jsx]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize.js]
[/Users/omarplummer/Repos/webpack-symlink-error-demo/consumer-app/vendor/react-dates/initialize.jsx]
 @ ../lerna-repo/packages/package-foo/lib/index.js 2:0-32
 @ ./app/bootstrap.js
 @ multi webpack-dev-server/client?http://localhost:3002 webpack/hot/only-dev-server ./app/bootstrap.js
 ```
 ```
 ✖ 1 problem (1 error, 0 warnings)

 @ multi webpack-dev-server/client?http://localhost:3002 webpack/hot/only-dev-server ./app/bootstrap.js app[2]

ERROR in ../lerna-repo/packages/package-foo/lib/index.js
Module not found: Error: Can't resolve 'react-dates/initialize' in '/Users/omarplummer/Repos/webpack-symlink-error-demo/lerna-repo/packages/package-foo/lib'
 @ ../lerna-repo/packages/package-foo/lib/index.js 2:0-32
 @ ./app/bootstrap.js
 @ multi webpack-dev-server/client?http://localhost:3002 webpack/hot/only-dev-server ./app/bootstrap.js
 ```